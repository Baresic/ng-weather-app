import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'images'
})
export class ImagesPipe implements PipeTransform {

  transform(value: number, args?: any): any {
    let source;

    let convert = value.toString();

    if( convert.startsWith("2") ){
        source = './assets/img/icons/thunder.svg'
    }
    if( convert.startsWith("3") ){
        source = './assets/img/icons/drizle.svg'
    }
    if( convert.startsWith("5") ){
        source = './assets/img/icons/rain.svg'
    }
    if( convert.startsWith("6") ){
        source = './assets/img/icons/snow.svg'
    }
    if( convert.startsWith("7") ){
        source = './assets/img/icons/mist.svg'
    }
    if( convert.startsWith("8") ){
        source = './assets/img/icons/sun.svg'
    }
    if( convert.startsWith("9") ){
        source = './assets/img/icons/tornado.svg'
    }

    return source;
  }

}
