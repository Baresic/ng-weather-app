import { Component, trigger, state, style, transition, animate } from '@angular/core';
import { Router } from '@angular/router';
import 'rxjs/add/operator/pairwise';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
  trigger('slideInOut', [
    state('in', style({
      transform: 'translate3d(0, 0, 0)'
    })),
    state('out', style({
      transform: 'translate3d(-101%, 0, 0)'
    })),
    transition('in => out', animate('300ms ease-in-out')),
    transition('out => in', animate('300ms ease-in-out'))
  ]),
]
})
export class AppComponent {
  menuState:string = 'out';
  private _opened: boolean = false;
  public isHome: boolean = false;
  public isActive: boolean  = false;

  toggleMenu() {
    this.menuState = this.menuState === 'out' ? 'in' : 'out';
    this.isActive = !this.isActive
  }

  constructor( public router: Router ){

    this.router.events.pairwise().subscribe((e) => {
             console.log(e);
             if(  this.router.url === '/home' ){
               console.log('home');
               this.isHome = true;
             }
             else{
               this.isHome = false;
             }
         });
  }
}
