import { Component, OnInit } from '@angular/core';
import { OpenweatherServiceService } from './openweather-service.service';
import { GeolocationService } from './geolocation.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {

  errorMessage: string;
  errorMessageMulti: string;
  weatherForecastData: any[];
  inputData: any[];

  constructor(private _weatherService:OpenweatherServiceService, private _route:Router, private _geo:GeolocationService ) { }


  ///CALL FOR MULTIPLE DAYS

  handleSucsessMultiple(data){
   this.weatherForecastData = data;
   //we call here publish method from service with wich we send data to other component
   this._weatherService.publish(this.weatherForecastData);

 }

  mutipleDays(cityName: string){
    this._weatherService.getWeatherForecast(cityName)
        .subscribe(data => { this.handleSucsessMultiple(data) },
                   error =>  this.errorMessageMulti = <any>error,

    );
    this._route.navigate(['/forecast']);

  }


  //GEOLOCATION
  handlecords(data){
    console.log(data);
    this.weatherForecastData = data;
    this._weatherService.publish(this.weatherForecastData);

  }


  sendCordinates(lat:number,lon:number){
    this._weatherService.getWeatherbyCordinates(lat,lon)
        .subscribe(data => { this.handlecords(data)},
                   error =>  this.errorMessageMulti = <any>error,
    );
  }

  getCurrentPosition() {

         if (navigator.geolocation) {
             this._geo.getCurrentPosition().forEach(
                 (position: Position) => {
                   this.sendCordinates(position.coords.latitude,position.coords.longitude);
                 }
             ).then(() => this._route.navigate(['/forecast'])).catch(
                 (error: PositionError) => {
                     if (error.code > 0) {
                         switch (error.code) {
                             case error.PERMISSION_DENIED:
                                 console.log('permission denied');
                                 break;
                             case error.POSITION_UNAVAILABLE:
                                 console.log('position unavailable');
                                 break;
                             case error.TIMEOUT:
                                 console.log('position timeout');
                                 break;
                         }
                        //  this.warning = true;
                     }
                 });
         } else {
             console.log("browser doesn't support geolocation");

         }

        //  this._route.navigate(['/forecast']);
     }


  ngOnInit() {

  }



}
