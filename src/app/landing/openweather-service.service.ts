import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subject }    from 'rxjs/Subject';
import { environment } from '../../environments/environment';
import 'rxjs/Rx';

@Injectable()
export class OpenweatherServiceService {


  //we define Subject which will help us transfer data between components

  //multidays api call
  public _multipleData = new Subject<any>();
  public event = this._multipleData.asObservable();


  //we define publish methon which we use in component from which we are transfering data
  public publish(data: any) {
     this._multipleData.next(data);
   }


constructor(private _http: Http) { }

getWeatherForecast(cityName): Observable<any>{

     return this._http.get(environment.baseUrl +'forecast/daily?q='+ cityName +'&mode=json&appid='+ environment.OPENWE_API_KEY +'&units=' + environment.units+'&cnt=6')
     .map(response => response.json())
     .catch(this.handleError);
}

getWeatherbyCordinates(lat, lon): Observable<any>{
     return this._http.get(environment.baseUrl +'forecast/daily?lat='+ lat +'&lon='+ lon +'&mode=json&appid='+ environment.OPENWE_API_KEY +'&units=' + environment.units+'&cnt=6')
     .map(response => response.json())
     .catch(this.handleError);
}


private handleError (error: any) {
  let errMsg: string;
  errMsg = error.message ? error.message : error.toString();
  console.error(errMsg);
  return Observable.throw(errMsg);
  }

}
