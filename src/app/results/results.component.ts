import { Component, OnInit } from '@angular/core';
import { OpenweatherServiceService } from '../landing/openweather-service.service';



@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss']
})
export class ResultsComponent implements OnInit {

  weather:any[];
  today:any[];
  retrived: boolean = false;


  constructor(private _weatherService:OpenweatherServiceService) {}

  ngOnInit() {
    //we subscribe to receive data from other component
    this._weatherService.event.subscribe((data) => {
      this.weather = data.list;
      this.today = data;
      this.retrived=true;
    });

  }


}
