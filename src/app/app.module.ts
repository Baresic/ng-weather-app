import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule , ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { ShareButtonsModule } from 'ngx-sharebuttons';

import { AppComponent } from './app.component';
import { LandingComponent } from './landing/landing.component';
import { ResultsComponent } from './results/results.component';
import { SidemenuComponent } from './sidemenu/sidemenu.component';



import { OpenweatherServiceService } from './landing/openweather-service.service';
import { GeolocationService } from './landing/geolocation.service';
import { DatePipe } from '@angular/common';
import { TemperaturePipe } from './pipes/temperature.pipe';
import { ImagesPipe } from './pipes/images.pipe';


@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    ResultsComponent,
    SidemenuComponent,
    TemperaturePipe,
    ImagesPipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    ShareButtonsModule.forRoot(),
    RouterModule.forRoot([
         { path: 'home', component: LandingComponent },
         { path: 'forecast', component: ResultsComponent },
         { path: '', redirectTo: 'home', pathMatch: 'full' },
         { path: '**', redirectTo: 'home', pathMatch: 'full' }
       ])

  ],
  providers: [ {provide: LocationStrategy, useClass: HashLocationStrategy}, OpenweatherServiceService, GeolocationService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
