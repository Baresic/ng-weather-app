// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  OPENWE_API_KEY: 'd18f0c5eac99a57df8e07733f3e7b5f5',
  baseUrl: 'http://api.openweathermap.org/data/2.5/',
  units:'metric'
};
